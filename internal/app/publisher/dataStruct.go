package publisher

type DataStruct struct {
	Subject string
	Message string
}