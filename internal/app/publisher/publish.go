package publisher

import (
	"github.com/nats-io/go-nats"
)

var Configuration ConfigurationStruct
var Data DataStruct

func Publish() error {
	connection, err := nats.Connect(Configuration.Address)
	if err != nil {
		return err
	}
	defer connection.Close()

	connection.Publish(Data.Subject, []byte(Data.Message))
	connection.Flush()
	
	err = connection.LastError()
	if err != nil {
		return err
	}

	return nil
}