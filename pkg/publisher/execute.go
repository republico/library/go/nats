package publisher

import (
	"gitlab.com/republico/library/go/nats/internal/app/publisher"
)

func Execute(address string, subject string, message string) error {
	publisher.Configuration = publisher.ConfigurationStruct{
		Address: address,
	}

	publisher.Data = publisher.DataStruct{
		Subject: subject,
		Message: message,
	}

	err := publisher.Publish()

	return err
}