package test

import (
	"testing"

	"gitlab.com/republico/library/go/nats/pkg/publisher"
)

func TestExecuteSingleSubject(t *testing.T) {
	address := "localhost:8222,localhost:8223,localhost:8224"
	subject := "republico__siconv"
	message := "[{\"id\": \"1\", \"name\": \"test1\"},{\"id\": \"2\", \"name\": \"test2\"},{\"id\": \"3\", \"name\": \"test3\"}]"

	err := publisher.Execute(address, subject, message)

	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestExecuteMultipleSubject(t *testing.T) {
	address1 := "localhost:8222,localhost:8223,localhost:8224"
	subject1 := "republico__siconv"
	message1 := "[{\"id\": \"1\", \"name\": \"test1\"},{\"id\": \"2\", \"name\": \"test2\"},{\"id\": \"3\", \"name\": \"test3\"}]"

	err1 := publisher.Execute(address1, subject1, message1)

	if err1 != nil {
		t.Errorf("Ocorreu um erro.")
	}

	address2 := "localhost:8222,localhost:8223,localhost:8224"
	subject2 := "republico__siafi"
	message2 := "[{\"id\": \"1\", \"name\": \"test1\"},{\"id\": \"2\", \"name\": \"test2\"},{\"id\": \"3\", \"name\": \"test3\"}]"

	err2 := publisher.Execute(address2, subject2, message2)

	if err2 != nil {
		t.Errorf("Ocorreu um erro.")
	}
}